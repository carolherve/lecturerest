package com.lecture.service;

import com.lecture.beans.User;
import com.lecture.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
@Service
public class UserServiceBean implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Collection<User> findAll() {
        Collection<User> users =userRepository.findAll();
        return users;
    }

    @Override
    public User findOne(Integer id) {
        return userRepository.findOne(id);
    }

    @Override
    public User create(User user) {
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        User userToUpdate = findOne(user.getIdUser());

        //verifier pas null
        //set les prorieters
        return userRepository.save(user);
    }

    @Override
    public void delete(Integer id) {

        userRepository.delete(id);
    }

	@Override
	public User findByName(String name) {
		ArrayList<User> users = (ArrayList<User>) this.findAll();
		for(int i = 0 ; i< users.size();i++){
			if (users.get(i).getFirstname().equals(name))
				return users.get(i);
		}
		return null;
	}
}