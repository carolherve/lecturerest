package com.lecture.service;

import com.lecture.beans.Dictionary;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface DictionaryService {

    Collection<Dictionary> findAll();

    Dictionary findOne(Integer id);

    Dictionary create(Dictionary dictionary);

    Dictionary update(Dictionary dictionary);

    void delete(Integer id);

}