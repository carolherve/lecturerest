package com.lecture.service;

import com.lecture.beans.Dictionary;
import com.lecture.beans.User;
import com.lecture.repositories.DictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class DictionaryServiceBean implements DictionaryService {

    @Autowired
    DictionaryRepository dictionaryRepository;

    @Override
    public Collection<Dictionary> findAll() {
        return dictionaryRepository.findAll();
    }

    @Override
    public Dictionary findOne(Integer id) {
        return dictionaryRepository.findOne(id);
    }

    @Override
    public Dictionary create(Dictionary dictionary) {
        return dictionaryRepository.save(dictionary);
    }

    @Override
    public Dictionary update(Dictionary dictionary) {
        Dictionary dictionaryToUpdate = findOne(dictionary.getIdDictionary());
        return dictionaryRepository.save(dictionaryToUpdate);

    }
    @Override
    public void delete(Integer id) {
        dictionaryRepository.delete(id);
    }
}