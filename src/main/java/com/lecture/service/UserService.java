package com.lecture.service;

import com.lecture.beans.User;
import org.springframework.stereotype.Service;

import java.util.Collection;
@Service
public interface UserService {

    public Collection<User> findAll();

    public User findOne(Integer id);

    public User create(User user);

    public  User update(User user);

    public void delete(Integer id);
    
    public User findByName(String name);
}
