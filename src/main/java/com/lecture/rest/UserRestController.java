package com.lecture.rest;

import com.lecture.beans.User;
import com.lecture.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;

@RestController
public class UserRestController {

	@Autowired
	private UserService userServiceBean;
	
	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<User>> getUsers() {
		Collection<User> users = userServiceBean.findAll();
		return new ResponseEntity<Collection<User>>(users, HttpStatus.OK);
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> getUserById(@PathVariable Integer id) {
		User user = userServiceBean.findOne(id);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/userName/{name}", method = RequestMethod.GET)
	public ResponseEntity<User> getUserByFirstName(@PathVariable String name) {
		User user = new User();
		ArrayList<User> users = (ArrayList<User>) userServiceBean.findAll();
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getFirstname().equals(name))
				user = users.get(i);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> addUser() {
		User user = new User();
		user.setFirstname("toto");
		user.setLastname("toto");
		userServiceBean.create(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	
	

}