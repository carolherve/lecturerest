package com.lecture.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lecture.beans.Dictionary;
import com.lecture.beans.Question;
import com.lecture.beans.Quiz;
import com.lecture.beans.Word;
import com.lecture.service.DictionaryService;

@RestController
public class QuizzController {

	
	@Autowired
	private DictionaryService dictionaryService;
	
	@RequestMapping(value = "/addQuizz", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Quiz> addQuiz() {
		Quiz quiz = new Quiz();
		Dictionary dict = new Dictionary();
		List<Dictionary> dicts = (List<Dictionary>) dictionaryService.findAll();
		dict = dicts.get(0);
		List<Word> words = (List<Word>) dict.getWords();
		Question q1 = new Question();
		q1.setIdQuestion(1);
		q1.setWord(words.get(0));
		List<Question> quests = new ArrayList<>();
		quests.add(q1);
		quiz.setIdQuiz(1);
		quiz.setQuestions(quests);

		return new ResponseEntity<Quiz>(quiz, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/Quizz", method = RequestMethod.GET)
	public ResponseEntity<Quiz> getQuiz(@RequestParam(value="langue", defaultValue="fra") String langue) {
		boolean found = false;
		Quiz quiz = new Quiz();
		Dictionary dict = new Dictionary();
		List<Dictionary> dicts = (List<Dictionary>) dictionaryService.findAll();
		
		for(int i = 0 ; i < dicts.size();i++){
			if(dicts.get(i).getDictionaryLanguage().getName().equals(langue)){
				dict = dicts.get(i);
				found = true;
			}
		}
		if(!found){
			dict = dicts.get(0);
		}
		List<Word> words = (List<Word>) dict.getWords();
		Question q1 = new Question();
		q1.setIdQuestion(1);
		q1.setWord(words.get(0));
		List<Question> quests = new ArrayList<>();
		quests.add(q1);
		quiz.setIdQuiz(1);
		quiz.setQuestions(quests);

		return new ResponseEntity<Quiz>(quiz, HttpStatus.OK);
	}

}
