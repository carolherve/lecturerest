package com.lecture.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lecture.beans.Dictionary;
import com.lecture.beans.Language;
import com.lecture.beans.Word;
import com.lecture.service.DictionaryService;
import com.lecture.service.UserService;

@RestController
public class DictionaryController {

	@Autowired
	private DictionaryService dictionaryServiceBean;

	@RequestMapping(value = "/dict", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Dictionary> addDictionay() {
		Language fra = new Language();
		Language eng = new Language();
		Language es = new Language();
		
		fra.setName("francais");
		eng.setName("english");
		es.setName("spanish");

		List<Word> frenchWords = new ArrayList<>();
		Word w1 = new Word();
		w1.setString("salut");
		frenchWords.add(w1);
		Dictionary dict = new Dictionary();
		dict.setWords(frenchWords);
		dict.setDictionaryLanguage(fra);

		Word w2 = new Word();
		w2.setString("hello");
		List<Word> engWords = new ArrayList<>();
		engWords.add(w2);

		Word w3 = new Word();
		w2.setString("cabesa");
		List<Word> esswords = new ArrayList<>();
		esswords.add(w3);

		Dictionary engDict = new Dictionary();
		engDict.setDictionaryLanguage(eng);
		engDict.setWords(engWords);

		Dictionary esDict = new Dictionary();
		esDict.setDictionaryLanguage(es);
		esDict.setWords(esswords);

		dictionaryServiceBean.create(dict);
		dictionaryServiceBean.create(engDict);
		dictionaryServiceBean.create(esDict);
		return new ResponseEntity<Dictionary>(dict, HttpStatus.OK);
	}

}
